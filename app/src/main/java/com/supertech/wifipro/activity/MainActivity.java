package com.supertech.wifipro.activity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.NetworkInfo;
import android.net.wifi.ScanResult;
import android.net.wifi.SupplicantState;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.Formatter;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;
import com.supertech.wifipro.R;
import com.supertech.wifipro.adapter.ListWiFiAdapter;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends AppCompatActivity {

    RecyclerView rcv_wifi;
    ListWiFiAdapter listWiFiAdapter;
    RecyclerView.LayoutManager layoutManager;
    Switch sw_on_off_wifi;
    WifiManager wifiManager;
    WifiScanReceiver wifiScanReceiver;
    List<ScanResult> wifiScanList=new ArrayList<>();
    ScanResult wifi_connecting;
    TextView txt_off_wifi_script;
    int networkId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        rcv_wifi= (RecyclerView) findViewById(R.id.rcv_wifi);
        sw_on_off_wifi= (Switch) findViewById(R.id.sw_on_off_wifi);
        txt_off_wifi_script= (TextView) findViewById(R.id.txt_off_wifi_script);
        wifiManager= (WifiManager) getSystemService(Context.WIFI_SERVICE);
        wifiScanReceiver=new WifiScanReceiver();
        layoutManager=new LinearLayoutManager(this);
        listWiFiAdapter=new ListWiFiAdapter(wifiScanList,rcv_wifi);
        listWiFiAdapter.setOnItemClickListener(new ListWiFiAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View v, int position) {
                wifi_connecting=wifiScanList.get(position);
                int status=isAccessPointSaved();
                if(status==0){
                    showDialogConnectWifi(wifi_connecting.SSID);
                }else if (status==1){
                    WifiInfo wifiInfo = wifiManager.getConnectionInfo();
                    updateDialogForget("Connected", WifiManager.calculateSignalLevel(wifiInfo.getRssi(), 5) + "/5", wifiInfo.getLinkSpeed() + "Mbps",
                            wifi_connecting.capabilities.substring(1, wifi_connecting.capabilities.indexOf("]") - 1), Formatter.formatIpAddress(wifiInfo.getIpAddress()));
                    showDialogForget();
                }else{

                }
//                if() {
//
//                }
            }
        });

        rcv_wifi.setLayoutManager(layoutManager);
        rcv_wifi.setAdapter(listWiFiAdapter);

        if(wifiManager.isWifiEnabled()) {
            wifiManager.startScan();
            sw_on_off_wifi.setChecked(true);
            rcv_wifi.setVisibility(View.VISIBLE);
            txt_off_wifi_script.setVisibility(View.GONE);
        }else{
            sw_on_off_wifi.setChecked(false);
            rcv_wifi.setVisibility(View.GONE);
            txt_off_wifi_script.setVisibility(View.VISIBLE);
        }

        sw_on_off_wifi.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    wifiManager.setWifiEnabled(isChecked);
                    if(!isChecked){
                        rcv_wifi.setVisibility(View.GONE);
                        txt_off_wifi_script.setVisibility(View.VISIBLE);
                        listWiFiAdapter.setWifiConnected("\"\"","");
                        listWiFiAdapter.notifyDataSetChanged();
                    }else{
                        rcv_wifi.setVisibility(View.VISIBLE);
                        txt_off_wifi_script.setVisibility(View.GONE);
                    }
            }
        });

        createDialogConnectWifi();
        createDialogForget();
    }

    protected void onPause() {
        unregisterReceiver(wifiScanReceiver);
        unregisterReceiver(wifiOpenreceiver);
        super.onPause();
    }

    protected void onResume() {
        registerReceiver(wifiScanReceiver, new IntentFilter(
                WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
        IntentFilter filter=new IntentFilter(WifiManager.WIFI_STATE_CHANGED_ACTION);
        filter.addAction(WifiManager.NETWORK_STATE_CHANGED_ACTION);
        filter.addAction(WifiManager.SUPPLICANT_STATE_CHANGED_ACTION);
        registerReceiver(wifiOpenreceiver, filter);
        super.onResume();
    }

    class WifiScanReceiver extends BroadcastReceiver {
        @SuppressLint("UseValueOf")
        public void onReceive(Context c, Intent intent) {
            List<WifiConfiguration> wifiConfigurations=wifiManager.getConfiguredNetworks();
            for(WifiConfiguration w: wifiConfigurations){
                Log.d("SSID",w.SSID);
                Log.d("status",w.status+"");
            }
            wifiScanList.clear();
            wifiScanList.addAll(wifiManager.getScanResults());
            listWiFiAdapter.notifyDataSetChanged();
        }
    }
    BroadcastReceiver wifiOpenreceiver=new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action=intent.getAction();
            if(action.equals(WifiManager.WIFI_STATE_CHANGED_ACTION)) {
                if(intent.getIntExtra(WifiManager.EXTRA_WIFI_STATE,0)==WifiManager.WIFI_STATE_ENABLED){
                    //Toast.makeText(context, "Open wifi", Toast.LENGTH_SHORT).show();
                    wifiManager.startScan();}
            }else if(action.equals(WifiManager.NETWORK_STATE_CHANGED_ACTION)){
                NetworkInfo networkInfo=intent.getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO);
                if(networkInfo.isConnected()){
                    WifiInfo wifiInfo = intent.getParcelableExtra(WifiManager.EXTRA_WIFI_INFO);
                    listWiFiAdapter.setWifiConnected(wifiInfo.getSSID(),wifiInfo.getBSSID());
                    listWiFiAdapter.notifyDataSetChanged();
                    Toast.makeText(context, "Sucess", Toast.LENGTH_SHORT).show();
                }else if(networkInfo.isConnectedOrConnecting()){
                    Toast.makeText(context, "Connecting...", Toast.LENGTH_SHORT).show();
                }else if(networkInfo.isFailover()){
                    Toast.makeText(context, "Faied", Toast.LENGTH_SHORT).show();
                    //wifiManager.removeNetwork(networkId);
                }
            }else if(action.equals(WifiManager.SUPPLICANT_STATE_CHANGED_ACTION)){
                //Toast.makeText(context,">>>>SUPPLICANT_STATE_CHANGED_ACTION<<<<<<", Toast.LENGTH_SHORT).show();
                SupplicantState supl_state = intent.getParcelableExtra(WifiManager.EXTRA_NEW_STATE);
                switch(supl_state){
                    case ASSOCIATED://Toast.makeText(context,"ASSOCIATED", Toast.LENGTH_SHORT).show();
                        break;
                    case ASSOCIATING://Toast.makeText(context,"ASSOCIATING", Toast.LENGTH_SHORT).show();
                        break;
                    case AUTHENTICATING://Toast.makeText(context,"Authenticating...", Toast.LENGTH_SHORT).show();
                        break;
                    case COMPLETED://Toast.makeText(context,"Connected", Toast.LENGTH_SHORT).show();
                        break;
                    case DISCONNECTED://Toast.makeText(context,"Disconnected", Toast.LENGTH_SHORT).show();
                        break;
                    case DORMANT://Toast.makeText(context,"DORMANT", Toast.LENGTH_SHORT).show();
                        break;
                    case FOUR_WAY_HANDSHAKE://Toast.makeText(context,"FOUR_WAY_HANDSHAKE", Toast.LENGTH_SHORT).show();
                        break;
                    case GROUP_HANDSHAKE://Toast.makeText(context,"GROUP_HANDSHAKE", Toast.LENGTH_SHORT).show();
                        break;
                    case INACTIVE://Toast.makeText(context,"INACTIVE", Toast.LENGTH_SHORT).show();
                        break;
                    case INTERFACE_DISABLED://Toast.makeText(context,"INTERFACE_DISABLED", Toast.LENGTH_SHORT).show();
                        break;
                    case INVALID://Toast.makeText(context,"INVALID", Toast.LENGTH_SHORT).show();
                        break;
                    case SCANNING://Toast.makeText(context,"SCANNING", Toast.LENGTH_SHORT).show();
                        break;
                    case UNINITIALIZED://Toast.makeText(context,"UNINITIALIZED", Toast.LENGTH_SHORT).show();
                        break;
                    default://Toast.makeText(context,"Unknown", Toast.LENGTH_SHORT).show();
                        break;

                }
                int supl_error=intent.getIntExtra(WifiManager.EXTRA_SUPPLICANT_ERROR, -1);
                if(supl_error==WifiManager.ERROR_AUTHENTICATING){
                    //Toast.makeText(context,"ERROR_AUTHENTICATING\", \"ERROR_AUTHENTICATING!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!", Toast.LENGTH_SHORT).show();
                    wifiManager.removeNetwork(networkId);
                }
            }
        }
    };
    Dialog connectDialog;
    EditText et_pass_wifi;
    Button btn_cancel_wifi,btn_connect_wifi;
    private void createDialogConnectWifi(){
        connectDialog = new Dialog(this);
        connectDialog.setCanceledOnTouchOutside(true);
        connectDialog.setContentView(R.layout.dialog_connect_wifi);
        et_pass_wifi = (EditText) connectDialog.findViewById(R.id.et_pass_wifi);
        btn_cancel_wifi = (Button) connectDialog.findViewById(R.id.btn_cancel_wifi);
        btn_connect_wifi = (Button) connectDialog.findViewById(R.id.btn_connect_wifi);
        btn_cancel_wifi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                connectDialog.dismiss();
            }
        });
        btn_connect_wifi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                connectWifibyPos(et_pass_wifi.getText().toString());
                connectDialog.dismiss();
            }
        });
    }

    private void showDialogConnectWifi(String wifiName){
        connectDialog.setTitle(wifiName);
        et_pass_wifi.setText("");
        connectDialog.show();
    }

    public void connectWifibyPos(String pass){
        listWiFiAdapter.setWifiConnected("\"\"","");
        WifiConfiguration config = new WifiConfiguration();

        String currentNetwork = wifiManager.getConnectionInfo().getSSID();
        if (currentNetwork != null && currentNetwork.equals(wifi_connecting.SSID))
        {
            Toast.makeText(this, "Already connected", Toast.LENGTH_SHORT).show();
            return;
        }

        config.SSID = String.format("\"%s\"", wifi_connecting.SSID);
        String firstCapabilities = wifi_connecting.capabilities.substring(1, wifi_connecting.capabilities.indexOf("]")-1);
        if(!firstCapabilities.equalsIgnoreCase("[ESS]")) {
            config.preSharedKey = String.format("\"%s\"", pass);
        }
        networkId = wifiManager.addNetwork(config);
        wifiManager.disconnect();
        wifiManager.enableNetwork(networkId, true);
    }

    /**
     *
     * @return 0 mean did't save
     *         1 mean connected
     *         2 mean saved but not connected
     */
    private int isAccessPointSaved(){
        List<WifiConfiguration> wifiConfigurations = wifiManager.getConfiguredNetworks();
        int i;
        int length=wifiConfigurations.size();
        for(i=0; i<length; i++){
            WifiConfiguration wifiConfiguration = wifiConfigurations.get(i);
            if(wifiConfiguration.SSID.substring(1,wifiConfiguration.SSID.length()-1).equals(wifi_connecting.SSID)){
                if(wifiConfiguration.status==0){
                    return 1;
                }else {
                    wifiManager.enableNetwork(wifiConfiguration.networkId, true);
                    return 2;
                }
            }
        }
        return 0;
    }

    Dialog dialog_forget;
    TextView tv_status,tv_signal,tv_link_speed,tv_security,tv_ip_address;
    Button btn_cancel_forget_wifi,btn_forget_wifi;
    private void createDialogForget(){
        dialog_forget = new Dialog(MainActivity.this);
        dialog_forget.setContentView(R.layout.dialog_forget_wifi);
        dialog_forget.setCanceledOnTouchOutside(true);

        tv_status = (TextView) dialog_forget.findViewById(R.id.tv_status);
        tv_signal = (TextView) dialog_forget.findViewById(R.id.tv_signal);
        tv_link_speed = (TextView) dialog_forget.findViewById(R.id.tv_link_speed);
        tv_security = (TextView) dialog_forget.findViewById(R.id.tv_security);
        tv_ip_address = (TextView) dialog_forget.findViewById(R.id.tv_ip_address);
        btn_cancel_forget_wifi = (Button) dialog_forget.findViewById(R.id.btn_cancel_forget_wifi);
        btn_forget_wifi = (Button) dialog_forget.findViewById(R.id.btn_forget_wifi);

        btn_cancel_forget_wifi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_forget.dismiss();
            }
        });
    }

    private void updateDialogForget(String status,String signal,String link_speed,String security,String ip_address){
        tv_status.setText(status);
        tv_signal.setText(signal);
        tv_link_speed.setText(link_speed);
        tv_security.setText(security);
        tv_ip_address.setText(ip_address);
    }

    private void showDialogForget(){
        dialog_forget.setTitle(wifi_connecting.SSID);
        dialog_forget.show();
    }
}
