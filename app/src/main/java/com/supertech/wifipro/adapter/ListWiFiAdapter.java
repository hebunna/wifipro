package com.supertech.wifipro.adapter;

import android.net.wifi.ScanResult;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.supertech.wifipro.R;
import com.supertech.wifipro.model.Wifi;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Bunna on 5/26/2015.
 */
public class ListWiFiAdapter extends RecyclerView.Adapter<ListWiFiAdapter.LViewHolder>{

    List<ScanResult> mWifis =new ArrayList<ScanResult>();
    String connected_ssid="\"\"",connected_bssid="";
    OnItemClickListener onItemClickListener =new OnItemClickListener() {
        @Override
        public void onItemClick(View v, int position) {

        }
    };
    RecyclerView rcv_list_wifi;

    public ListWiFiAdapter(List<ScanResult> wifis,RecyclerView rcv_list_wifi){
        this.mWifis =wifis;
        this.rcv_list_wifi = rcv_list_wifi;
    }

    @Override
    public LViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        return new LViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_rcv_wifi,null));
    }

    @Override
    public void onBindViewHolder(LViewHolder lViewHolder, int i) {
        ScanResult wifi= mWifis.get(i);
        Log.d("capabilities "+i,wifi.capabilities);
        Log.d("BSSID"+i,wifi.BSSID);
        Log.d("SSID",connected_ssid.substring(1,connected_ssid.length()-1));
        lViewHolder.setName(wifi.SSID);
        if(connected_ssid.substring(1,connected_ssid.length()-1).equals(wifi.SSID)&&connected_bssid.equals(wifi.BSSID)) {
            lViewHolder.setType("Connected");
        }else{
            lViewHolder.setType("");
        }

    }

    public void setWifiConnected(String connected_ssid,String connected_bssid){
        this.connected_ssid = connected_ssid;
        this.connected_bssid = connected_bssid;
    }

    @Override
    public int getItemCount() {
        return mWifis.size();
    }

    class LViewHolder extends RecyclerView.ViewHolder{

        TextView txt_wifi_name,txt_wifi_type;

        public LViewHolder(View itemView) {
            super(itemView);
            txt_wifi_name= (TextView) itemView.findViewById(R.id.txt_wifi_name);
            txt_wifi_type= (TextView) itemView.findViewById(R.id.txt_wifi_type);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClickListener.onItemClick(v,rcv_list_wifi.getChildPosition(v));
                }
            });
        }

        public void setName(String name){
            txt_wifi_name.setText(name);
        }

        public void setType(String type){
            txt_wifi_type.setText(type);
        }
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public interface OnItemClickListener{
        void onItemClick(View v,int position);
    }
}
