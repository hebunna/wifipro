package com.supertech.wifipro.model;

/**
 * Created by Bunna on 5/26/2015.
 */
public class Wifi {
    String name;
    String type;

    public Wifi(String name, String type) {
        this.name = name;
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}
